# Page building Exercise

## Overview
This exercise will challenge you to build two page layouts from pre-built CSS. The goal of the exercise is to create the pages without modifying any of the existing CSS.

## Starter files
The two completed page layouts should look like the screen shots in the `/screenshots` directory when you are finished. There are six total images: two layouts and three different screen sizes. There are two shell pages with a header and footer to start from.

The starter HTML pages reside in `/public`. There's an `index.html` to easily link to the pages and there's a `layout-1.html` and `layout-2.html` file.

There is a CSS Styleguide that documents the relevant CSS modules that have been included in the project. Each CSS module includes example HTML markup to see how the CSS can be used as well as the modules visual output. The CSS styleguide resides in `/public/styles/styleguide/`. Use this as a reference to understand how the individual modules work independently and to see what modifier classes are available.

You may open the files directly in your browser for previewing or you may use the livereload utility that is included in the project. To use the livereload utility run `npm install` and then `gulp`. This command will start a local static server that runs at `http://localhost:8000/` (there's no need to include the `/public` directory in the url).
